package com.kangjusang.threadtest;

import android.util.Log;

public class SubRunnable implements Runnable {
    @Override
    public void run() {
        try {
            Log.d("SubRunnable", "This thread lasts for 5 seconds");
            Thread.sleep(5000);
            Log.d("SubRunnable", "This thread will end immediately");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
