package com.kangjusang.threadtest;

import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                final HandlerThread handlerThread = new HandlerThread("HandlerThreadName");
                handlerThread.start();

                final LooperThread looperThread = new LooperThread();
                looperThread.start();

                Thread threadToDelay = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        Handler handler = new Handler(handlerThread.getLooper());
                        handler.post(new Runnable(){
                            @Override
                            public void run() {
                                Log.d("MainActivity", "This is to be printed on HandlerThread");
                            }
                        });

//                        looperThread.mHandler.sendEmptyMessage(10);
//                        looperThread.mHandler.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                Log.d("MainActivity", "This is to be printed on LooperThread");
//                            }
//                        });
                    }
                });
                threadToDelay.setDaemon(true);
                threadToDelay.start();


//                SubThread subThread = new SubThread("SubThread1");
//                subThread.setDaemon(true);
//                subThread.start();


//                SubThread subThread1 = new SubThread("SubThread2");
//                subThread1.setDaemon(true);
//                subThread1.start();


//                SubRunnable subRunnable = new SubRunnable();
//                Thread thread = new Thread(subRunnable);
//                thread.setDaemon(true);
//                thread.start();


            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
