package com.kangjusang.threadtest;

import android.util.Log;

public class SubThread extends Thread {

    public SubThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        super.run();

        try {
            Log.d("SubThread", "This thread lasts for 5 seconds");
            Thread.sleep(5000);
            Log.d("SubThread", "This thread will end immediately");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
