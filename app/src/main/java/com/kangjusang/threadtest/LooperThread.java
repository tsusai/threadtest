package com.kangjusang.threadtest;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

public class LooperThread extends Thread {

    Handler mHandler;

    @Override
    public void run() {
        Looper.prepare();
        mHandler = new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                Log.d("LooperThread", "handleMessage called : " + msg.toString());
            }
        };
        Looper.loop();
        Log.d("LooperThread", "This will not be printed unless this thread terminates.");
    }
}
